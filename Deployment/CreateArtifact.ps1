param([string]$SolutionDir)
$7z = "C:\Program Files\7-Zip\7z.exe"
$archiveName = [io.path]::combine($SolutionDir, "Deployment\Lineyschikov.VpnManager.zip")
$dll = [io.path]::combine($SolutionDir, "Lineyschikov.VpnManager.PowerShell\bin\Release\*Vpn*.dll")
$psd = [io.path]::combine($SolutionDir, "Deployment\*.psd1")
$readme = [io.path]::combine($SolutionDir, "README.md");
rm $archiveName
& $7z a $archiveName $dll $psd $readme