$SolutionDir = "D:\WORK\Lineyschikov.VpnManager"
$dll = [io.path]::combine($SolutionDir, "Lineyschikov.VpnManager.PowerShell/bin/Release/*Vpn*.dll")
$psd = [io.path]::combine($SolutionDir, "Deployment\*.psd1")
$modulePath = [io.path]::combine($env:PSModulePath.Split(";")[0], "Lineyschikov.VpnManager")
if (!(Test-Path -path $modulePath)) {New-Item $modulePath -Type Directory}
Copy-Item -Path $dll -Destination $modulePath
Copy-Item -Path $psd -Destination $modulePath