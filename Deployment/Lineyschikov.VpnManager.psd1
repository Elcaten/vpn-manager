# Манифест модуля для модуля "VpnManager".
# Создано: a.lineyshikov
# Дата создания: 06.10.2016

@{

# Файл модуля сценария или двоичного модуля, связанный с этим манифестом.
RootModule = 'Lineyschikov.VpnManager.PowerShell.dll'

# Номер версии данного модуля.
ModuleVersion = '1.0'

# Уникальный идентификатор данного модуля
GUID = 'f6106fe9-da93-46c2-88f4-bce775e00361'

# Автор данного модуля
Author = 'a.lineyshikov'

# Заявление об авторских правах на модуль
Copyright = '(c) 2016 a.lineyshikov. Все права защищены.'

# Функции для экспорта из данного модуля
FunctionsToExport = '*'

# Командлеты для экспорта из данного модуля
CmdletsToExport = '*'

# Переменные для экспорта из данного модуля
VariablesToExport = '*'

# Псевдонимы для экспорта из данного модуля
AliasesToExport = '*'

}

