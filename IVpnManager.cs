namespace Lineyschikov.VpnManager
{
    /// <summary>
    /// �������� VPN
    /// </summary>
    public interface IVpnManager
    {
        /// <summary>
        /// ����������
        /// </summary>
        void Connect();

        /// <summary>
        /// ���������
        /// </summary>
        void Disconnect();

        /// <summary>
        /// ����� �������� ������ �� VPN �������
        /// </summary>
        int TimeOut { get; set; }
    }
}