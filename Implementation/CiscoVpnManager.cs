using System.Windows.Forms;
using static Lineyschikov.VpnManager.VpnHelper;

namespace Lineyschikov.VpnManager
{
    /// <summary>
    /// Менеджер Cisco AnyConnect
    /// </summary>
    public class CiscoVpnManager : IVpnManager
    {
        private static readonly string DefaultCiscoExePath = "C:\\Program Files (x86)\\Cisco\\Cisco AnyConnect Secure Mobility Client\\vpnui.exe";
        private readonly string _ciscoExePath;
        private readonly string _password;

        public int TimeOut { get; set; } = 6000;

        public CiscoVpnManager(string password, string ciscoExePath = null)
        {
            _password = password;
            _ciscoExePath = GetPath(ciscoExePath, DefaultCiscoExePath);
        }

        public void Connect()
        {
            KillProcess(_ciscoExePath);
            StartProcess(_ciscoExePath, timeout: TimeOut);
            SendKeys.SendWait($"{_password}{{ENTER}}");
        }

        public void Disconnect()
        {
            KillProcess(_ciscoExePath);
        }
    }
}