﻿using System.Threading;
using System.Windows.Forms;
using static Lineyschikov.VpnManager.VpnHelper;

namespace Lineyschikov.VpnManager
{
    /// <summary>
    /// Менеджер Citrix Acess Gateway
    /// </summary>
    public class CitrixVpnManager : IVpnManager
    {
        private static readonly string DefaultIeExePath = "C:\\Program Files\\Internet Explorer\\iexplore.exe";
        private static readonly string DefaultCitrixExePath = "C:\\Program Files\\Citrix\\Secure Access Client\\nsload.exe";
        private static string _ieExePath;
        private static string _citrixExePath;
        private static string _username;
        private static string _password;

        public int TimeOut { get; set; } = 5000;

        public CitrixVpnManager(string username, string password, string citrixExePath = null, string ieExePath = null)
        {
            _username = username;
            _password = password;
            _citrixExePath = GetPath(citrixExePath, DefaultCitrixExePath);
            _ieExePath = GetPath(ieExePath, DefaultIeExePath);
        }

        public void Connect()
        {
            KillProcess(_citrixExePath);
            StartProcess(_citrixExePath, "/noDisplayLogin");
            var ieProcess = StartProcess(_ieExePath, "-k https://in.yota.ru/vpn/index.html ", 5000);
            SendWait("{F12}", 2000);    //Open dev toools
            SendWait("{TAB}{TAB}{TAB}{TAB}"); //Select user agent combobox
            SendWait(" "); //Click it
            SendWait("{HOME}{DOWN}{DOWN}{DOWN}{DOWN}{ENTER}"); //Select IE7 user agent
            SendWait($"{_username}{{TAB}}{_password}{{ENTER}}", TimeOut); //Send credentials
            ieProcess?.Kill();
        }

        public void Disconnect()
        {
            KillProcess(_citrixExePath);       
        }

        private static void SendWait(string keys, int timeout = 1000)
        {
            SendKeys.SendWait(keys);
            Thread.Sleep(timeout);
        }
    }
}