﻿namespace Lineyschikov.VpnManager
{
    /// <summary>
    /// Менеджер VPN по-умолчанию
    /// </summary>
    public class DefaultVpnManager : IVpnManager
    {
        public int TimeOut { get; set; }

        public void Connect()
        {
        }

        public void Disconnect()
        {
        }
    }
}