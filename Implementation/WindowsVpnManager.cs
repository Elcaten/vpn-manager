﻿using System.Diagnostics;

namespace Lineyschikov.VpnManager
{
    /// <summary>
    /// Менеджер Microsoft VPN
    /// </summary>
    public class WindowsVpnManager : IVpnManager
    {
        private readonly string _vpnName;
        private const int ErrorSuccess = 0;

        public int TimeOut { get; set; }

        public WindowsVpnManager(string vpnName)
        {
            _vpnName = vpnName;
        }

        public void Connect()
        {
            var process = Process.Start("rasdial.exe", _vpnName);
            if (process == null)
                throw new VpnException("Не удалось запустить процесс rasdial.exe");
            process.WaitForExit();
            if (process.ExitCode != ErrorSuccess)
                throw new VpnException($"Процессу rasdial.exe не удалось подключится к {_vpnName}. Код ошибки: {process.ExitCode}");
        }

        public void Disconnect()
        {
            var process = Process.Start("rasdial.exe", _vpnName + " /disconnect");
            if (process == null)
                throw new VpnException("Не удалось запустить процесс rasdial.exe");
            process.WaitForExit();
            if (process.ExitCode != ErrorSuccess)
                throw new VpnException($"Процесс rasdial.exe завершился с ошибкой. Код ошибки: {process.ExitCode}");
        }
    }
}