using System.Management.Automation;

namespace Lineyschikov.VpnManager.PowerShell
{
    [Cmdlet(VerbsCommunications.Connect, "Cisco", DefaultParameterSetName = "Connect")]
    public class ConnectCiscoCmdlet : Cmdlet
    {
        [Parameter(Mandatory = true, Position = 0, ParameterSetName = "Connect")]
        public string Password { get; set; }

        [Parameter(Mandatory = false, ParameterSetName = "Connect")]
        public int? Timeout { get; set; }

        [Parameter(Mandatory = false, ParameterSetName = "Connect")]
        public string CiscoExePath { get; set; }

        [Parameter(Mandatory = true, ParameterSetName = "Disconnect")]
        public SwitchParameter Disconnect { get; set; }

        protected override void ProcessRecord()
        {
            var vpnManager = new CiscoVpnManager(Password, CiscoExePath);
            if (Disconnect.IsPresent)
                vpnManager.Disconnect();
            else 
                vpnManager.Connect();
        }
    }
}