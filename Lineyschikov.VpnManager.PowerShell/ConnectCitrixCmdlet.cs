using System;
using System.Management.Automation;

namespace Lineyschikov.VpnManager.PowerShell
{
    [Cmdlet(VerbsCommunications.Connect, "Citrix", DefaultParameterSetName = "Connect")]
    public class ConnectCitrixCmdlet : Cmdlet
    {
        [Parameter(Mandatory = true, Position = 0, ParameterSetName = "Connect")]
        public string Username { get; set; }

        [Parameter(Mandatory = true, Position  = 1, ParameterSetName = "Connect")]
        public string Password { get; set; }

        [Parameter(Mandatory = false, ParameterSetName = "Connect")]
        public int? Timeout { get; set; }

        [Parameter(Mandatory = false, ParameterSetName = "Connect")]
        public string CitrixExePath { get; set; }

        [Parameter(Mandatory = false, ParameterSetName = "Connect")]
        public string IeExePath { get; set; }

        [Parameter(Mandatory = true, ParameterSetName = "Disconnect")]
        public SwitchParameter Disconnect { get; set; }

        protected override void ProcessRecord()
        {
            var vpnManager = new CitrixVpnManager(Username, Password, CitrixExePath, IeExePath);
            if (Disconnect.IsPresent)
                vpnManager.Disconnect();
            else
                vpnManager.Connect();
        }
    }
}