using System.Management.Automation;
using System.Reflection;

namespace Lineyschikov.VpnManager.PowerShell
{
    [Cmdlet(VerbsCommunications.Connect, "WindowsVpn", DefaultParameterSetName = "Connect")]
    public class ConnectWindowsVpnCmdlet : Cmdlet
    {
        [Parameter(Mandatory = true, Position = 0, ParameterSetName = "Connect")]
        [Parameter(Mandatory = true, Position = 0, ParameterSetName = "Disconnect")]
        public string Name { get; set; }

        [Parameter(Mandatory = false, ParameterSetName = "Connect")]
        public int? Timeout { get; set; }

        [Parameter(Mandatory = true, Position = 1, ParameterSetName = "Disconnect")]
        public SwitchParameter Disconnect { get; set; }

        protected override void ProcessRecord()
        {
            var vpnManager = new WindowsVpnManager(Name);
            if (Disconnect.IsPresent)
                vpnManager.Disconnect();
            else
                vpnManager.Connect();
        }
    }
}