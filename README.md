```
██╗   ██╗██████╗ ███╗   ██╗              ███╗   ███╗ █████╗ ███╗   ██╗ █████╗  ██████╗ ███████╗██████╗ 
██║   ██║██╔══██╗████╗  ██║              ████╗ ████║██╔══██╗████╗  ██║██╔══██╗██╔════╝ ██╔════╝██╔══██╗
██║   ██║██████╔╝██╔██╗ ██║    █████╗    ██╔████╔██║███████║██╔██╗ ██║███████║██║  ███╗█████╗  ██████╔╝
╚██╗ ██╔╝██╔═══╝ ██║╚██╗██║    ╚════╝    ██║╚██╔╝██║██╔══██║██║╚██╗██║██╔══██║██║   ██║██╔══╝  ██╔══██╗
 ╚████╔╝ ██║     ██║ ╚████║              ██║ ╚═╝ ██║██║  ██║██║ ╚████║██║  ██║╚██████╔╝███████╗██║  ██║
  ╚═══╝  ╚═╝     ╚═╝  ╚═══╝              ╚═╝     ╚═╝╚═╝  ╚═╝╚═╝  ╚═══╝╚═╝  ╚═╝ ╚═════╝ ╚══════╝╚═╝  ╚═╝
```

### Для чего этот репозиторий? ####
Он позволяет подключать всевозможные VPN, используемые на проектах Никфорта (Cisco, Citrix), а также стандартный VPN винды.
- - - -

### Как использовать? ####
#### Из кода:
1. [Скачать](https://ci.appveyor.com/api/projects/Elcaten/vpn-manager/artifacts/Lineyschikov.VpnManager.zip)
2. Подключить библиотеку **Lineyschikov.VpnManager.dll** к проекту
3. Юзать

#### Из PowerShell:

##### Установка: #####


```
#!PowerShell

$f = [IO.Path]::Combine($env:TEMP, (Get-Date).Ticks)
wget "https://ci.appveyor.com/api/projects/Elcaten/vpn-manager/artifacts/Lineyschikov.VpnManager.zip" -OutFile $f
& $($env:ProgramFiles + "\7-zip\7z.exe") x $f $("-o"+ $env:PSModulePath.Split(";")[0] + "\Lineyschikov.VpnManager") -aoa
rm $f
```
##### Примеры использования: #####

```
#!PowerShell
PS> Connect-WindowsVpn <vpnName> [-Disconnect]
PS> Connect-Cisco <password>
PS> Connect-Cisco -Disconnect
PS> Connect-Citrix <username> <password> 
PS> Connect-Citrix -Disconnect

```
- - - -
### Нюансы
* В Cisco AnyConnect должна стоять галка **Start VPN when AnyConnect is started**
* Connect-Citrix забагован, желательно держать курсор в поле юзернейма
- - -
![BuildStatus](https://ci.appveyor.com/api/projects/status/0j1s88dwg9pulxgn?svg=true)