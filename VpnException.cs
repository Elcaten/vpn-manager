﻿using System;

namespace Lineyschikov.VpnManager
{
    /// <summary>
    /// Ошибка VPN подключения
    /// </summary>
    public class VpnException : Exception
    {
        public VpnException()
        {
        }

        public VpnException(string message) : base(message)
        {
        }

        public VpnException(string format, params object[] args) : base(string.Format(format, args))
        {
        }

        public VpnException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}