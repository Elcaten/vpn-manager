﻿using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;

namespace Lineyschikov.VpnManager
{
    public static class VpnHelper
    {
        /// <summary>
        /// Останавливает процесс
        /// </summary>
        /// <param name="processPath">Путь к файлу</param>
        public static void KillProcess(string processPath)
        {
            var processName = Path.GetFileNameWithoutExtension(processPath);
            var process = Process.GetProcessesByName(processName).FirstOrDefault();
            if (process != null) process.Kill();
        }

        /// <summary>
        /// Запускает процесс
        /// </summary>
        /// <param name="processPath">Путь к файлу</param>
        /// <param name="args">Аргументы</param>
        /// <param name="timeout">Время ожидания после запуска</param>
        public static Process StartProcess(string processPath, string args = null, int? timeout = null)
        {
            var process = Process.Start(processPath, args);
            if (process == null) throw new VpnException("Can't start {0}", processPath);
            if (timeout.HasValue) Thread.Sleep(timeout.Value);
            return process;
        }

        /// <summary>
        /// Проверяет существование пути, возвращает путь по-умолчанию в случае несуществующего пути
        /// </summary>
        /// <param name="path">Путь</param>
        /// <param name="defaultpath">Путь по умолчанию</param>
        public static string GetPath(string path, string defaultpath)
        {
            if (string.IsNullOrWhiteSpace(path))
            {
                if (File.Exists(defaultpath)) return defaultpath;
                throw new VpnException("Can't find {0}. Try specifying path manually", defaultpath);
            }
            if (File.Exists(path)) return path;
            throw new VpnException("Can't find {0}. Make sure path is correct", path);
        }
    }
}